package tv.mediagenix.aktrestdemo.rest

import io.mockk.every
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import tv.mediagenix.aktrestdemo.application.service.GreeterService
import tv.mediagenix.aktrestdemo.application.service.GreetingRequest
import tv.mediagenix.aktrestdemo.application.service.GreetingResponse

@ExtendWith(MockKExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class GreeterRestControllerTest {

    @InjectMockKs
    private lateinit var restController: GreeterRestController

    @MockK
    private lateinit var service: GreeterService

    @Test
    fun `hello without target`() {
        val response = restController.hello(null)
        assertEquals(-1, response.id)
        assertEquals("stranger", response.addressee)
    }

    @Test
    fun `hello with target`() {
        val target = "Santa"
        val response = restController.hello(target)
        assertEquals(-1, response.id)
        assertEquals(target, response.addressee)
    }

    @Test
    fun greet() {
        val id = 42
        every { service.addGreeting(any()) } answers {
            val request = arg<GreetingRequest>(0)
            GreetingResponse(id, request.timestamp, request.addressee)
        }

        val target = "Santa"
        val response = restController.greet(target)
        assertEquals(id, response.id)
        assertEquals(target, response.addressee)

        verify(exactly = 1) { service.addGreeting(match { it.addressee == target }) }
    }

    @Test
    fun findGreets() {
        // TODO
    }

    @Test
    fun deleteGreet() {
        // TODO
    }
}