package tv.mediagenix.aktrestdemo

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class AktrestdemoApplication

fun main(args: Array<String>) {
	runApplication<AktrestdemoApplication>(*args)
}
