package tv.mediagenix.aktrestdemo.rest

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import tv.mediagenix.aktrestdemo.application.service.GreeterService
import tv.mediagenix.aktrestdemo.application.service.GreetingRequest
import tv.mediagenix.aktrestdemo.application.service.GreetingResponse
import tv.mediagenix.aktrestdemo.rest.json.GreetingJson
import tv.mediagenix.aktrestdemo.rest.json.toJson
import java.util.*

@RestController
@RequestMapping("greeter")
class GreeterRestController(
    private val greeterService: GreeterService
) {

    @GetMapping("hello")
    fun hello(@RequestParam(required = false) from: String?): GreetingJson {
        val sender = from ?: "stranger"
        return GreetingJson(-1, Date(), sender)
    }

    @PostMapping("{addressee}")
    fun greet(@PathVariable addressee: String): GreetingJson =
        greeterService.addGreeting(GreetingRequest(Date(), addressee)).toJson()

    @GetMapping("{addressee}")
    fun findGreets(@PathVariable addressee: String): Sequence<GreetingJson> =
        greeterService.findGreetings(addressee).map(GreetingResponse::toJson)

    @DeleteMapping("{greetId}")
    fun deleteGreet(@PathVariable greetId: Int): ResponseEntity<Unit> =
        if (greeterService.deleteById(greetId)) ResponseEntity.ok().build()
        else ResponseEntity.notFound().build()
}

