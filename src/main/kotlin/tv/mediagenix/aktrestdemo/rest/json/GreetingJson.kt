package tv.mediagenix.aktrestdemo.rest.json

import tv.mediagenix.aktrestdemo.application.service.GreetingResponse
import java.util.*

data class GreetingJson(
    val id: Int,
    val timestamp: Date,
    val addressee: String
)

fun GreetingResponse.toJson() = GreetingJson(id, timestamp, addressee)