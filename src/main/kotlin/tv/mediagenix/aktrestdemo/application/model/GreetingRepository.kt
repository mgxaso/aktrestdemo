package tv.mediagenix.aktrestdemo.application.model

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface GreetingRepository : CrudRepository<Greeting, Int> {
    fun findAllByWho(who: String): Iterable<Greeting>
}