package tv.mediagenix.aktrestdemo.application.service

import tv.mediagenix.aktrestdemo.application.model.Greeting
import java.util.*

data class GreetingRequest(val timestamp: Date, val addressee: String)

fun GreetingRequest.toEntity() = Greeting(timestamp, addressee)