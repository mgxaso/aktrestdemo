package tv.mediagenix.aktrestdemo.application.service

import tv.mediagenix.aktrestdemo.application.model.Greeting
import java.util.*

data class GreetingResponse(val id: Int, val timestamp: Date, val addressee: String)

fun Greeting.toResponse() = GreetingResponse(id!!, timestamp, who)