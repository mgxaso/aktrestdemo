package tv.mediagenix.aktrestdemo.application.service

import org.springframework.stereotype.Component
import tv.mediagenix.aktrestdemo.application.model.Greeting
import tv.mediagenix.aktrestdemo.application.model.GreetingRepository

interface GreeterService {
    fun addGreeting(greeting: GreetingRequest): GreetingResponse

    fun findGreetings(addressee: String): Sequence<GreetingResponse>

    fun deleteById(greetId: Int): Boolean
}

@Component
internal class GreeterServiceImplementation(
    private val greetingRepository: GreetingRepository
) : GreeterService {
    override fun addGreeting(greeting: GreetingRequest) =
        greetingRepository.save(greeting.toEntity()).toResponse()

    override fun findGreetings(addressee: String) = greetingRepository
        .findAllByWho(addressee).asSequence().map(Greeting::toResponse)

    override fun deleteById(greetId: Int) = try {
        greetingRepository.deleteById(greetId)
        true
    } catch (e: Exception) {
        false
    }
}